﻿using Android.App;
using Android.Content;

namespace VolumioCallMute
{
    public class NotificationHelper
    {
        public static void ShowNotification(Context context, string message)
        {
            var builder = new Notification.Builder(context)
                .SetContentTitle("DEBUG")
                .SetSmallIcon(Android.Resource.Drawable.SymActionCall)
                .SetContentText(message);

            var manager = (NotificationManager) context.GetSystemService(Context.NotificationService);
            manager.Notify(101, builder.Build());
        }
    }
}