﻿using System.Net.Http;
using Android.App;
using Android.Content;
using Android.Preferences;
using Android.Telephony;
using Newtonsoft.Json;

namespace VolumioCallMute
{
    public class PhoneCallDetector : PhoneStateListener
    {
        private readonly Context _context;
        private bool _oncall;
        private int _lastVolume;

        public PhoneCallDetector(Context context)
        {
            _context = context;
        }

        public override async void OnCallStateChanged(CallState state, string incomingNumber)
        {
            var prefs = PreferenceManager.GetDefaultSharedPreferences(Application.Context);
            var address = prefs.GetString("address", string.Empty);
            
            if (string.IsNullOrWhiteSpace(address))
                return;
            var client = new HttpClient();
            switch (state)
            {
                case CallState.Offhook:
                    var devState = await client.GetStringAsync($"http://{address}/api/v1/getState");
                    var data = JsonConvert.DeserializeObject<StateSlim>(devState);

                    var currentVolume = data?.Volume ?? 0;
                    _lastVolume = currentVolume;
                    currentVolume = (int)(currentVolume * 0.2);
                    _oncall = true;
                    await client.GetAsync($"http://{address}/api/v1/commands/?cmd=volume&volume={currentVolume}");
                    break;
                case CallState.Idle when _oncall:
                    await client.GetAsync($"http://{address}/api/v1/commands/?cmd=volume&volume={_lastVolume}");
                    _oncall = false;
                    break;
            }

            base.OnCallStateChanged(state, incomingNumber);
        }
    }
}