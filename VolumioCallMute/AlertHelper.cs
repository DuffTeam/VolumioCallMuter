﻿using System;
using System.Threading.Tasks;
using Android.App;

namespace VolumioCallMute
{
    public static class AlertHelper
    {
        public static Task AlertAsync(string text)
        {
            var tcs = new TaskCompletionSource<bool>();

            var ad = new AlertDialog.Builder(MainActivity.Instance)
                .SetMessage(text)
                .SetPositiveButton("Ok", (sender, args) => tcs.SetResult(true))
                .Create();

            void OnDismiss(object sender, EventArgs args)
            {
                ad.DismissEvent -= OnDismiss;
                tcs.TrySetResult(true);
            }

            ad.DismissEvent += OnDismiss;
            ad.Show();

            return tcs.Task;
        }
    }
}