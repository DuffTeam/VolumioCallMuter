﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Telephony;

namespace VolumioCallMute
{
    [Service]
    public class PhoneCallService : Service
    {
        public override IBinder OnBind(Intent intent)
        {
            throw new System.NotImplementedException();
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            base.OnStartCommand(intent, flags, startId);

            var detector = new PhoneCallDetector(this);
            var manager = (TelephonyManager) GetSystemService(TelephonyService);
            manager.Listen(detector, PhoneStateListenerFlags.CallState);

            return StartCommandResult.Sticky;
        }
    }
}