﻿using System;
using System.Linq;
using System.Net.NetworkInformation;
using Android.App;
using Android.Content;
using Android.Widget;
using Android.OS;
using Android.Preferences;
using Android.Text;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;

namespace VolumioCallMute
{
    [Activity(Label = "VolumioCallMute", MainLauncher = true)]
    public class MainActivity : Activity
    {
        private ProgressBar _progressBar;
        private EditText _addressText;
        private Button _connectButton;
        private RelativeLayout _progress;

        protected MainActivity(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
            Instance = this;
        }

        public MainActivity()
        {
            Instance = this;
        }

        internal static MainActivity Instance { get; private set; }
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            _progressBar = FindViewById<ProgressBar>(Resource.Id.progressBar);
            _progress = FindViewById<RelativeLayout>(Resource.Id.progress);
            _addressText = FindViewById<EditText>(Resource.Id.addressBox);
            _addressText.Text = "10.17.2.146";
            _addressText.TextChanged += OnTextChanged;
            _connectButton = FindViewById<Button>(Resource.Id.connectButton);
            _connectButton.Click += OnConnect;

            if (ServiceRunning)
                return;

            var start = new Intent(this, typeof(PhoneCallService));
            StartService(start);
        }

        private void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var text = _addressText.Text.Trim();
            _connectButton.Enabled = !string.IsNullOrWhiteSpace(text);
        }

        private async void OnConnect(object sender, EventArgs e)
        {
            var imm = (InputMethodManager) GetSystemService(InputMethodService);
            imm.HideSoftInputFromWindow(_addressText.WindowToken, HideSoftInputFlags.None);

            using (var ping = new Ping())
            {
                var address = _addressText.Text.Trim();
                _progress.Visibility = ViewStates.Visible;
                var success = await ping.SendPingAsync(address, 5 * 1000);
                _progress.Visibility = ViewStates.Gone;

                if (success.Status == IPStatus.Success)
                {
                    PreferenceManager.GetDefaultSharedPreferences(Application.Context)
                        .Edit()
                        .PutString("address", address)
                        .Apply();

                    await AlertHelper.AlertAsync("Success!");
                    return;
                }

                await AlertHelper.AlertAsync("Could not connect to Volumio");
            }
        }

        private bool ServiceRunning
        {
            get
            {
                var manager = (ActivityManager) GetSystemService(ActivityService);
#pragma warning disable 618
                var instance = manager.GetRunningServices(int.MaxValue)
#pragma warning restore 618
                    .FirstOrDefault(info => info.Service.ShortClassName.EndsWith(nameof(PhoneCallService)));
                return instance != null;
            }
        }
    }
}

