﻿using Android.App;
using Android.Content;

namespace VolumioCallMute
{
    [BroadcastReceiver]
    [IntentFilter(new[] {Intent.ActionBootCompleted})]
    public class StartupReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            var serviceStart = new Intent(context, typeof(PhoneCallService));
            context.StartService(serviceStart);
        }
    }
}